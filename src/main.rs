use itertools::Itertools;

fn transform(original: String) -> String {
    let mut exp = String::from("");

    for (key, val) in original.chars().group_by(|&c| c).into_iter() {
        exp.push_str(&val.count().to_string());
        exp.push(key);
    }
    exp
}

fn main() {
    let mut val = String::from("22164224441");

    for _ in 0..40 {
        val = transform(val);
    }
    //let exp = transform(String::from("111223"));

    println!("{}", val.len());
}
